
let cacheFunction = require('../cacheFunction.cjs')

function add(a, b) {
    return a+ b;
  }

  let no = 5
  
  const cachedAdd = cacheFunction(add);



  console.log(cachedAdd(1, 2)); // Output: Calculating sum... 3
  console.log(cachedAdd(1, 2)); // Output: 3 (Cached)
  console.log(cachedAdd(2, 3)); // Output: Calculating sum... 5
  console.log(cachedAdd(2, 3)); // Output: 5 (Cached)
  