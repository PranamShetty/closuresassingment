
let counterFactory = require("../counterFactory.cjs")

const counter1 = counterFactory();
const counter2 = counterFactory();

console.log(counter1.increment()); // 1
console.log(counter1.increment()); // 2
console.log(counter2.increment()); // 1
console.log(counter1.decrement()); // 1
console.log(counter2.decrement()); // 0
  