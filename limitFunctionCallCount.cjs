function limitFunctionCallCount(cb, n) {

  if(typeof cb !== "function"){
    throw new Error("call back is not the function")
}

if(typeof n !== "number"){
  throw new Error("n is not a number ")
}

        let count = 0;
        function limitedCb(...args) {
          if (count < n) {
            count++;
            return cb(...args);
          }else{
          return null
          }
        }

      
        return limitedCb;



}

module.exports = limitFunctionCallCount
      
