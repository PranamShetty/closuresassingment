function cacheFunction(cb) {
    const cache = {};

    if(typeof cb !== "function"){
        throw new Error("callback is not a function");
    }
  
    return function (...args) {
      const key = JSON.stringify(args);
  
      if (key in cache) {
        return cache[key];
      } else {
        const result = cb(...args);
        cache[key] = result;
        console.log(cache)
        return result;
      }
    };
  }


  module.exports = cacheFunction